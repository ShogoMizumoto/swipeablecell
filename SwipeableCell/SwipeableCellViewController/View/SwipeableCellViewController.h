//
//  SwipeableCellViewController.h
//  SwipeableCell
//
//  Created by ShogoMizumoto on 2014/06/12.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//



#import "SwipeableCell.h"

@interface SwipeableCellViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SwipeableCellDelegate>

@property NSMutableArray *mDataSourceArray;

@property (weak, nonatomic) IBOutlet UITableView *uTableView;

- (IBAction)tapReload:(id)sender;

@end

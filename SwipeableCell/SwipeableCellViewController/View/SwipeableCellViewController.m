//
//  SwipeableCellViewController.m
//  SwipeableCell
//
//  Created by ShogoMizumoto on 2014/06/12.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

#import "SwipeableCellViewController.h"

static NSString *const kDataSourceText_0 = @"あなたは！あなただけは！";
static NSString *const kDataSourceText_1 = @"それでも！守りたい世界があるんだ！";
static NSString *const kDataSourceText_2 = @"そんな、あなたの理屈！";
static NSString *const kDataSourceText_3 = @"やめてよね、本気でケンカしたら";
static NSString *const kDataSourceText_4 = @"サイが僕に適うはずないだろ";
static NSString *const kDataSourceText_5 = @"違う、人は・・・人はそんなもんじゃない！";
static NSString *const kDataSourceText_6 = @"やめろと言ったろ、死にたいのか！？";
static NSString *const kDataSourceText_7 = @"キラ・ヤマト、";
static NSString *const kDataSourceText_8 = @"フリーダム、";
static NSString *const kDataSourceText_9 = @"いきます！";

@implementation SwipeableCellViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.automaticallyAdjustsScrollViewInsets = NO;

    [self initTableView];
}

- (void)initTableView
{
    NSLog(@"call initTableView.");

    // Init DataSource.
    self.mDataSourceArray = nil;
    self.mDataSourceArray = [NSMutableArray arrayWithObjects:
                    [[CellItem alloc] initWithTitle:kDataSourceText_0 index:0],
                    [[CellItem alloc] initWithTitle:kDataSourceText_1 index:1],
                    [[CellItem alloc] initWithTitle:kDataSourceText_2 index:2],
                    [[CellItem alloc] initWithTitle:kDataSourceText_3 index:3],
                    [[CellItem alloc] initWithTitle:kDataSourceText_4 index:4],
                    [[CellItem alloc] initWithTitle:kDataSourceText_5 index:5],
                    [[CellItem alloc] initWithTitle:kDataSourceText_6 index:6],
                    [[CellItem alloc] initWithTitle:kDataSourceText_7 index:7],
                    [[CellItem alloc] initWithTitle:kDataSourceText_8 index:8],
                    [[CellItem alloc] initWithTitle:kDataSourceText_9 index:9],
                    nil];
}

- (void)resetCellItemIndex
{
    NSLog(@"call resetCellItemIndex.");

    for (NSUInteger cnt = 0; cnt < [self.mDataSourceArray count]; cnt++) {
        CellItem *item = [self.mDataSourceArray objectAtIndex:cnt];
        item.mIndex = cnt;
    }
}

- (IBAction)tapReload:(id)sender
{
    [self initTableView];
    [self.uTableView reloadData];
}






#pragma mark- --- Delegate UITableView

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"call cellForRowAtIndexPath. IndexPath:%d", indexPath.row);

    CellItem *feedItem = [self.mDataSourceArray objectAtIndex:indexPath.row];

    SwipeableCell *cell = (SwipeableCell *) [tableView dequeueReusableCellWithIdentifier:kXibFileName];
    if (cell == nil) {
        UINib *nib = [UINib nibWithNibName:(kXibFileName) bundle:nil];
        NSArray *array = [nib instantiateWithOwner:self options:nil];
        cell = [array objectAtIndex:0];
    }

    return [cell updateCell:feedItem delegate:self];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"call numberOfRowsInSection. Seciotn:%d", section);

    return self.mDataSourceArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSLog(@"call numberOfSectionsInTableView.");
    return 1;
}






#pragma mark- --- Delegate TableCell

- (void)swipeCellWithEvent:(SwipeableEvent)event cellItem:(CellItem *)cellItem
{
    NSLog(@"call swipeCellWithEvent. Event:%d CellItem:%@", event, cellItem);

    NSInteger cellIndex;

    switch (event) {
        case SwipeableEventSwipeNothing:
            break;

        case SwipeableEventSwipeStart:
            break;

        case SwipeableEventSwiping:
            break;

        case SwipeableEventSwipeEnd:
            break;

        case SwipeableEventSwipeCancel:
            break;

        case SwipeableEventExistAnimationStart:
            break;

        case SwipeableEventExistAnimationEnd:
            break;

        case SwipeableEventDeleteAnimationStart:
            break;

        case SwipeableEventDeleteAnimationEnd:
            cellIndex = cellItem.mIndex;

            [self.mDataSourceArray removeObjectAtIndex:cellIndex];

            // セル番号が変わるので、それに応じて各CellItem内のセル番号も更新する
            [self resetCellItemIndex];

            // iOS標準の、セル削除アニメーション処理
            [self.uTableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:cellIndex inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

@end

//
//  SwipeableCell.h
//  SwipeableCell
//
//  Created by ShogoMizumoto on 2014/06/12.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

#import "CellItem.h"

typedef NS_ENUM(NSInteger, SwipeableEvent)
{
    SwipeableEventSwipeNothing,         // スワイプしていない状態
    SwipeableEventSwipeStart,           // 画面タップされてスワイプが開始された
    SwipeableEventSwiping,              // スワイプ中
    SwipeableEventSwipeEnd,             // 画面から指が離れてスワイプが終了された
    SwipeableEventSwipeCancel,          // 端末画面外へのスワイプ等、なんらかの方法でキャンセルされた
    SwipeableEventExistAnimationStart,  // セルを元の位置に戻すアニメーションの開始
    SwipeableEventExistAnimationEnd,    // セルを元の位置に戻すアニメーションの終了
    SwipeableEventDeleteAnimationStart, // セルを削除するアニメーションの開始
    SwipeableEventDeleteAnimationEnd,   // セルを削除するアニメーションの終了
};

@protocol SwipeableCellDelegate

@required
- (void)swipeCellWithEvent:(SwipeableEvent)event cellItem:(CellItem *)cellItem;

@end


extern NSString *const kXibFileName;


@interface SwipeableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView  *uViewCellBase; // セルのベースとなる View
@property (weak, nonatomic) IBOutlet UIView  *uViewDragArea; // ドラッグ可能エリア
@property (weak, nonatomic) IBOutlet UILabel *uLabelTitle;

@property (assign, nonatomic) id<SwipeableCellDelegate> mDelegateSwipeable;
@property CellItem *mCellItem;

- (id)updateCell:(CellItem *)cellItem delegate:(id<SwipeableCellDelegate>)delegate;

@end

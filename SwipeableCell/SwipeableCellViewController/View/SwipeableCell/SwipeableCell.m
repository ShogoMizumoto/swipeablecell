//
//  SwipeableCell.m
//  SwipeableCell
//
//  Created by ShogoMizumoto on 2014/06/12.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

#import "SwipeableCell.h"

NSString *const kXibFileName = @"SwipeableCell";

static NSInteger kDragLimitPointX = 100;
static NSInteger kDragLimitSpeed  = 800;

static NSString *kAnimationKey_ReturnCell = @"ReturnCell";
static NSString *kAnimationKey_SwipeCell  = @"SwipeCell";

@interface SwipeableCell()
{
    CGRect mDefaultCellBaseRect; // 「初期時/スワイプによって削除された時」の状態によって保持する、Cell のベースとなる View の座標
    BOOL   isDragEndAnimating;   // ドラッグ終了後の、削除/戻るアニメーション中か判定するフラグ
}
@end

@implementation SwipeableCell

@synthesize mDelegateSwipeable;

/**
* セル内の View 等を更新する
*
*
* TableView は一定数の TableViewCell を使いまわして表示処理を行っているため、
* 前回の TableViewCell を更新する必要がある
*/
- (id)updateCell:(CellItem *)cellItem delegate:(id <SwipeableCellDelegate>)delegate
{
    self.mCellItem          = cellItem;
    self.mDelegateSwipeable = delegate;

    self.uLabelTitle.text = self.mCellItem.mTitle;

    // スワイプ可能対象 View に ドラッグイベントを付与する
    UIPanGestureRecognizer *drag = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(onDragEvent:)];
    [self.uViewDragArea addGestureRecognizer:drag];

    // 初期時の Cell のベースとなる View の座標を保持
    mDefaultCellBaseRect = self.uViewCellBase.frame;

    return self;
}

/**
* ドラッグ中に毎回呼び出されるメソッド
*/
- (void)onDragEvent:(UIPanGestureRecognizer *)drag
{
    // ドラッグ終了後の、削除/戻るアニメーション中であれば、ここで return
    if (isDragEndAnimating) {
        return;
    }

    // 各種、ドラッグ中の情報を取得する
    CGPoint dragLocationPoint = [drag locationInView:self.window];  // 端末画面上でのドラッグ中の座標
    CGPoint dragRelativePoint = [drag translationInView:drag.view]; // ドラッグ開始座標から今どこをドラッグしているか判別する座標
    CGPoint dragSpeed         = [drag velocityInView:drag.view];    // X/Y軸に応じたドラッグ時の速さ

    NSLog(@"DragLocationPointInWindow:%@", NSStringFromCGPoint(dragLocationPoint));
    NSLog(@"DragRelativePoint        :%@", NSStringFromCGPoint(dragRelativePoint));
    NSLog(@"DragSpeed                :%@", NSStringFromCGPoint(dragSpeed));


    /**
    * 現在のスワイプ状態を、Delegate に通知する
    */
    SwipeableEvent event = SwipeableEventSwipeNothing;

    switch (drag.state) {
        case UIGestureRecognizerStateBegan:
            event = SwipeableEventSwipeStart;
            break;

        case UIGestureRecognizerStateChanged:
            event = SwipeableEventSwiping;
            break;

        case UIGestureRecognizerStateEnded:
            event = SwipeableEventSwipeEnd;
            break;

        case UIGestureRecognizerStateCancelled:
            event = SwipeableEventSwipeCancel;
            break;

        case UIGestureRecognizerStateFailed:
            event = SwipeableEventSwipeCancel;
            break;

        case UIGestureRecognizerStatePossible: // マルチタップシーケンス開始イベント
            event = SwipeableEventSwipeStart;  // Multi-tap is null and void? // FIXME
            break;

        default:
            event = SwipeableEventSwipeNothing;
            break;
    }
    [self.mDelegateSwipeable swipeCellWithEvent:event cellItem:self.mCellItem];


    // ドラッグ開始座標からドラッグ中座標までの距離が上下それぞれ、セルの高さ分の範囲内であればドラッグを許可する判定処理
    CGFloat minLimitDragPointY = self.uViewCellBase.frame.size.height * -1;
    CGFloat maxLimitDragPointY = self.uViewCellBase.frame.size.height;

    if (dragRelativePoint.y < minLimitDragPointY || dragRelativePoint.y > maxLimitDragPointY) {
        // ドラッグ中座標が上下それぞれセルの高さ分だけ超えているため、何もしない
        // NOP.

    } else {
        // ドラッグ開始座標からの相対距離分だけ、座標を移動する
        self.uViewCellBase.frame = CGRectMake(
                mDefaultCellBaseRect.origin.x + dragRelativePoint.x,
                mDefaultCellBaseRect.origin.y,
                mDefaultCellBaseRect.size.width,
                mDefaultCellBaseRect.size.height);
    }

    /**
    * 画面から指を話したら、UIGestureRecognizerStateEnded が状態として取得できるため、
    * その状態だった場合、ドラッグ終了時メソッドを呼ぶ
    */
    if (drag.state == UIGestureRecognizerStateEnded) {
        [self onDragEventEndProcess:dragSpeed];
    }
}

/**
* ドラッグ終了後の処理を担うメソッド
*
* セルのアニメーションを決める
*/
- (void)onDragEventEndProcess:(CGPoint)dragSpeed
{
    // ドラッグ終了後の、削除/戻るアニメーション中か判定するフラグを YES にする
    isDragEndAnimating = YES;

    // ドラッグ開始時のX座標と、ドラッグ終了時のX座標を取得する
    CGFloat   beforeX      = mDefaultCellBaseRect.origin.x;
    CGFloat   afterX       = self.uViewCellBase.frame.origin.x;

    // 移動した距離を求める
    NSInteger moveDistance = (NSInteger) floor(fabs(beforeX - afterX)); // 移動距離の値は絶対値とし、小数点切り捨て

    // 移動したときの速さを求める
    NSInteger moveSpeed    = (NSInteger) floor(fabs(dragSpeed.x));      // 速さの値は絶対値とし、小数点切り捨て

    // 一定の距離を移動しているのであれば、セルを削除したと見なす
    BOOL isCellBaseOverTheLimitPointX = moveDistance > kDragLimitPointX;

    // ドラッグ終了時のドラッグの速さが一定値を上回っているか判定
    BOOL isDragSpeedOverTheLimitSpeed = moveSpeed    > kDragLimitSpeed;

    // 一定の距離をドラッグし、且つ一定のスピードを持ってドラッグしているならば、セルを削除するアニメーションを開始する
    if (isCellBaseOverTheLimitPointX && isDragSpeedOverTheLimitSpeed) {
        [self startAnimationDeleteCell:dragSpeed];

    } else {
        // 上記の条件に満たしていないため、セルを元の位置に戻すアニメーションを開始する
        [self startAnimationReturnCell];
    }
}






#pragma mark- --- Animation Method

/**
* セルを削除するアニメーションの開始
*/
- (void)startAnimationDeleteCell:(CGPoint)dragSpeed
{
    CGPoint toPoint;

    /**
    * CellBase の X座標が元のX軸より左右のどちらに存在するか判定し
    * 左なら画面左に向けて、右なら画面右に向けて画面外へ移動アニメーションを開始する
    */
    CGFloat toPointX;
    if (mDefaultCellBaseRect.origin.x < self.uViewCellBase.frame.origin.x) {
        toPointX = [[UIScreen mainScreen] bounds].size.width;
    } else {
        toPointX = 0 - self.uViewCellBase.frame.size.width;
    }

    toPoint = CGPointMake(
            toPointX                          + self.uViewCellBase.frame.size.width  / 2,
            self.uViewCellBase.frame.origin.y + self.uViewCellBase.frame.size.height / 2);

    /**
    * 移動先までの距離と速さからアニメーション時間を求める
    * なるべく、ドラッグした速さを維持しつつアニメーションをしているように見せるため
    */
    CGFloat duration     = (self.uViewCellBase.frame.origin.x - toPointX) / dragSpeed.x;

    CABasicAnimation *animMove = [CABasicAnimation animationWithKeyPath:@"position"];
    animMove.delegate            = self;
    animMove.duration            = duration;
    animMove.repeatCount         = 0;
    animMove.autoreverses        = NO;
    animMove.removedOnCompletion = NO;
    animMove.fillMode            = kCAFillModeForwards;
    animMove.timingFunction      = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animMove.toValue             = [NSValue valueWithCGPoint:toPoint];

    // アニメーション後の座標を保持する
    mDefaultCellBaseRect.origin   = CGPointMake(
            toPoint.x - self.uViewCellBase.frame.size.width  / 2,
            toPoint.y - self.uViewCellBase.frame.size.height / 2);

    // アニメーション開始
    [self.uViewCellBase.layer addAnimation:animMove forKey:kAnimationKey_SwipeCell];

    // デリゲートに、セルを削除するアニメーションが開始したことを通知する
    [self.mDelegateSwipeable swipeCellWithEvent:SwipeableEventDeleteAnimationStart cellItem:self.mCellItem];
}

/**
* セルを元の位置に戻すアニメーションを開始
*/
- (void)startAnimationReturnCell
{
    CGPoint toPoint;

    toPoint = CGPointMake(
            mDefaultCellBaseRect.origin.x + self.uViewCellBase.frame.size.width  / 2,
            mDefaultCellBaseRect.origin.y + self.uViewCellBase.frame.size.height / 2);

    /**
    * 元の座標まで移動アニメーションを開始する
    */
    CABasicAnimation *animMove = [CABasicAnimation animationWithKeyPath:@"position"];
    animMove.delegate            = self;
    animMove.duration            = 0.35;
    animMove.repeatCount         = 0;
    animMove.autoreverses        = NO;
    animMove.removedOnCompletion = NO;
    animMove.fillMode            = kCAFillModeForwards;
    animMove.timingFunction      = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animMove.toValue             = [NSValue valueWithCGPoint:toPoint];

    // アニメーション後の座標を保持する
    mDefaultCellBaseRect.origin   = CGPointMake(
            toPoint.x - self.uViewCellBase.frame.size.width  / 2,
            toPoint.y - self.uViewCellBase.frame.size.height / 2);

    // アニメーション開始
    [self.uViewCellBase.layer addAnimation:animMove forKey:kAnimationKey_ReturnCell];

    // デリゲートに、セルを元の位置に戻すアニメーションが開始したことを通知する
    [self.mDelegateSwipeable swipeCellWithEvent:SwipeableEventExistAnimationStart cellItem:self.mCellItem];
}

/**
* 各種アニメーションが完了したときに呼ばれるメソッド
*
*
* どのアニメーションが完了したかは、アニメーション対象の View.layer からアニメーションインスタンスを取得し
* 引数の CAAnimation インスタンスと同じか判定する
*/
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.uViewCellBase.layer animationForKey:kAnimationKey_SwipeCell]) {

        // セルを削除するアニメーション完了時

        // CAAnimation インスタンスを対象 View から削除する
        [self.uViewCellBase.layer removeAnimationForKey:kAnimationKey_SwipeCell];

        // アニメーション後の座標に移動させる
        self.uViewCellBase.frame = mDefaultCellBaseRect;

        // ドラッグ終了後の、削除/戻るアニメーション中か判定するフラグを NO にする
        isDragEndAnimating = NO;

        /**
        * !!! このセル自体を透明にする
        * ViewController 側から UITableView のセルを削除するアニメーションを行うが、
        * 削除したセルの下に位置するセルが、上へ迫り上がるアニメーションになったときに、
        * 削除したセルの下に隠れてしまうような現象が起きてしまうため、
        * ここで削除予定のセルを透明にすることで、自然なアニメーションにしている
        * 本現象を確認するなら、以下の opacity に 0.0f を代入する処理をコメントアウトすれば確認可能
        */
        self.layer.opacity = 0.0f;

        // デリゲートに、セルを削除するアニメーションが完了したことを通知する
        [self.mDelegateSwipeable swipeCellWithEvent:SwipeableEventDeleteAnimationEnd cellItem:self.mCellItem];

    }
    else
    if (anim == [self.uViewCellBase.layer animationForKey:kAnimationKey_ReturnCell]) {

        // セルを元の位置に戻すアニメーション完了時

        // CAAnimation インスタンスを対象 View から削除する
        [self.uViewCellBase.layer removeAnimationForKey:kAnimationKey_ReturnCell];

        // アニメーション後の座標に移動させる
        self.uViewCellBase.frame = mDefaultCellBaseRect;

        // ドラッグ終了後の、削除/戻るアニメーション中か判定するフラグを NO にする
        isDragEndAnimating = NO;

        // デリゲートに、セルを元の位置に戻すアニメーションが完了したことを通知する
        [self.mDelegateSwipeable swipeCellWithEvent:SwipeableEventExistAnimationEnd cellItem:self.mCellItem];

    }
}

@end

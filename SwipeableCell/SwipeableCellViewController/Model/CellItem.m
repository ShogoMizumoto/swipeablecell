//
//  CellItem.m
//  SwipeableCell
//
//  Created by ShogoMizumoto on 2014/06/12.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

#import "CellItem.h"

@implementation CellItem

- (id)initWithTitle:(NSString *)title index:(NSInteger)index
{
    self = [super init];
    if (self) {
        self.mTitle = title;
        self.mIndex = index;
    }
    return self;
}

@end

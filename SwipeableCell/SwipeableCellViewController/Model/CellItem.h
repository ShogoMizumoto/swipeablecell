//
//  CellItem.h
//  SwipeableCell
//
//  Created by ShogoMizumoto on 2014/06/12.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//



@interface CellItem : NSObject

@property NSString  *mTitle;
@property NSInteger  mIndex;

- (id)initWithTitle:(NSString *)title index:(NSInteger)index;

@end
